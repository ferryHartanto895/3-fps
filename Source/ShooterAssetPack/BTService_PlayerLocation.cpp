// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_PlayerLocation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Pawn.h"


UBTService_PlayerLocation::UBTService_PlayerLocation()
{
	//Behavior Tree にノードを作ります。
	NodeName = "Update Player Location";
}

void UBTService_PlayerLocation::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	//プレイヤのポジションを探します。
	APawn* playerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	//プレイヤがなかったら場合
	if (playerPawn == nullptr)
	{
		return;
	}
	//プレイヤのポジションはblackboardに設定します。
	OwnerComp.GetBlackboardComponent()->SetValueAsVector(GetSelectedBlackboardKey(), playerPawn->GetActorLocation());
}
