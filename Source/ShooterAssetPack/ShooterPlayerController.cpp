// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterPlayerController.h"
#include "TimerManager.h"
#include "Blueprint/UserWidget.h"

void AShooterPlayerController::BeginPlay()
{
	Super::BeginPlay();

	//HUDの設定
	HUD = CreateWidget(this, HUDClass);
	if (HUD != nullptr)
	{
		HUD->AddToViewport();
	}
}

void AShooterPlayerController::GameHasEnded(class AActor* EndGameFocus, bool bIsWinner)
{
	Super::GameHasEnded(EndGameFocus,bIsWinner);

	//勝つの時HUD消えます
	HUD->RemoveFromViewport();
	if (bIsWinner)
	{
		//WinHUD呼びます
		UUserWidget* winScreen = CreateWidget(this, WinScreenClass);
		if (winScreen != nullptr)
		{
			winScreen->AddToViewport();
		}
	}
	else
	{
		//LoseHUD呼びます
		UUserWidget* loseScreen = CreateWidget(this, LoseScreenClass);
		if (loseScreen != nullptr)
		{
			loseScreen->AddToViewport();
		}
	}

	GetWorldTimerManager().SetTimer(restartTimer, this, &APlayerController::RestartLevel, restartDelay);
}