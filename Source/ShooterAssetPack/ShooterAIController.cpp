// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAIController.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "ShooterCharacter.h"

void AShooterAIController::BeginPlay()
{
	Super::BeginPlay();

	//BahviorTreeを確認します
	if (AIBehavior != nullptr)
	{
		//BehaviorTreeを始まります
		RunBehaviorTree(AIBehavior);

		APawn* playerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
		//StartLocationは現在のポジションを設定します
		GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"), GetPawn()->GetActorLocation());
	}
}

void AShooterAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

}

bool AShooterAIController::IsDead() const
{
	//AIがなかったら場合
	AShooterCharacter* controlledChar = Cast<AShooterCharacter>(GetPawn());
	if (controlledChar != nullptr)
	{
		return controlledChar->IsDead();
	}

	return true;
}