// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
// Sets default values
AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//gunはgame sceneの中に表示されるために
	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(root);
	//gunのmesh変数を作ります。
	mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(root);
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGun::PullTrigger()
{
	//粒子を作ります
	UGameplayStatics::SpawnEmitterAttached(muzzleFlash, mesh, TEXT("MuzzleFlashSocket"));
	//gunの音
	UGameplayStatics::SpawnSoundAttached(muzzleSound,mesh, TEXT("MuzzleSocket"));
	
	FHitResult hit;
	
	FVector shotDir;

	bool bSuccess = GunTrace(hit, shotDir);
	if (bSuccess)
	{
		//impactの時粒子と音
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), impactEffect, hit.Location, shotDir.Rotation());
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), impactSound, hit.Location);
		AActor* hitActor = hit.GetActor();
		if (hitActor != nullptr)
		{
			//ヒットしたっらダメージの関数を呼びます
			FPointDamageEvent damageEvent(damage, hit, shotDir, nullptr);
			AController* ownerController = GetOwnerController();
			hitActor->TakeDamage(damage, damageEvent, ownerController, this);
		}
	}
}

bool AGun::GunTrace(FHitResult& Hit, FVector& ShotDirection)
{
	AController* ownerController = GetOwnerController();
	if (ownerController == nullptr)
	{
		return false;
	}


	FVector loc;
	FRotator rot;
	//プレイヤの見える方をもらいました
	ownerController->GetPlayerViewPoint(loc, rot);
	ShotDirection = -rot.Vector();

	FVector end = loc + rot.Vector() * maxRange;
	FCollisionQueryParams params;
	//自分の身体をヒットしないために
	params.AddIgnoredActor(this);
	params.AddIgnoredActor(GetOwner());

	//gunのヒットする場所を設定する
	return GetWorld()->LineTraceSingleByChannel(Hit, loc, end, ECollisionChannel::ECC_GameTraceChannel11, params);
}

AController* AGun::GetOwnerController() const
{
	//gunのコントローラー
	APawn* ownerPawn = Cast<APawn>(GetOwner());
	if (ownerPawn == nullptr)return nullptr;
	return ownerPawn->GetController();
}

