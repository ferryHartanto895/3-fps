// Fill out your copyright notice in the Description page of Project Settings.


#include "KillEmAllGameMode.h"
#include "EngineUtils.h"
#include "GameFramework/Controller.h"
#include "ShooterAIController.h"

void AKillEmAllGameMode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);

	//プレイヤかAIがいったらEndgameがfalseにします
	APlayerController* playerController = Cast<APlayerController>(PawnKilled->GetController());
	if (playerController != nullptr)
	{
		EndGame(false);
	}

	//なかったら場合trueになります
	for (AShooterAIController* controller : TActorRange<AShooterAIController>(GetWorld()))
	{
		if (!controller->IsDead())
		{
			return;
		}
	}
	EndGame(true);
}

void AKillEmAllGameMode::EndGame(bool bIsWinner)
{
	//AIかプレイヤが死ぬ時、ゲームが終わります
	for (AController* controller : TActorRange<AController>(GetWorld()))
	{
		bool bWinner = controller->IsPlayerController() == bIsWinner;
		controller->GameHasEnded(controller->GetPawn(), bWinner);
	}
}
